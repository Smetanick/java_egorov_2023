package ru.itis.repositories;

import ru.itis.models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentsRepository {
    List<Student> findAll();

    void save(Student student);

    Optional<Student> findById(Long id);
}
