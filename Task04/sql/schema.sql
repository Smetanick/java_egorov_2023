drop table if exists student_course;
drop table if exists student;
drop table if exists lesson;
drop table if exists course;

-- создание таблицы
create table student
(
    id         bigserial primary key, -- идентификатор строки - всегда уникальный
    first_name varchar(20),
    last_name  varchar(20),
    age        integer check ( age > 18 and age < 120)
);

create table course
(
    id          serial primary key,
    title       varchar(20),
    description varchar(100),
    start_date  date,
    finish_date date
);

create table lesson
(
    id          serial primary key,
    name        varchar(20),
    start_time  time,
    finish_time time,
    day_of_week char(20)
);

-- изменение таблицы (добавление внешнего ключа)
alter table lesson
    add course_id bigint;
alter table lesson
    add foreign key (course_id) references course (id);

alter table student
    add phone_number varchar(20) not null default '';

create table student_course
(
    student_id bigint,
    course_id  bigint,
    foreign key (student_id) references student (id),
    foreign key (course_id) references course (id)
);
