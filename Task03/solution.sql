--1
select model, speed, hd
from pc c
where c.price < 500

--2
select distinct(c.maker)
from product c
where c.type = 'printer'

--3
select distinct(c.maker)
from product c
where c.type = 'printer'

--4
select *
from printer
where color = 'y'

--5
select model, speed, hd
from pc
where (cd = '12x' or cd = '24x')
  and price < 600

--6
select distinct(c.maker), l.speed
from product c
         join laptop l on (c.model = l.model and l.hd >= 10)
where c.type = 'laptop'

--7
select model, price
from pc
where model in (select model from product where maker = 'B')
union
select model, price
from laptop
where model in (select model from product where maker = 'B')
union
select model, price
from printer
where model in (select model from product where maker = 'B')

--8
select maker
from product
where type = 'pc'
    except
select maker
from product
where type = 'laptop'

--9
select distinct maker
from product
where type = 'pc'
  and model in (select model from pc where speed >= 450)

--10
select c.model, c.price
from printer c
where c.price in (select max(price) from printer)

--11
select avg(speed)
from pc

--12
select avg(speed)
from laptop
where price > 1000

--13
select avg(speed)
from pc
where model in (select model from product where maker = 'A')

--14
select c.class, c.name, m.country
from ships c
         join classes m on c.class = m.class and m.numguns >= 10

--15
select c.hd
from pc c
group by c.hd
having count(hd) >= 2

--16
select distinct (a.model), b.model, a.speed, a.ram
from pc a,
     pc b
where a.model > b.model
  and a.speed = b.speed
  and a.ram = b.ram

--17
select distinct c.type, c.model, l.speed
from product c
         join laptop l on (c.model = l.model and type = 'laptop' and l.speed < all (select speed from pc))

--18
select distinct c.maker, l.price
from product c
         join printer l on (c.model = l.model and l.color = 'y')
where l.price in (select min(price) from printer where color = 'y')

--19
select distinct c.maker, avg(l.screen)
from product c
         join laptop l on (c.model = l.model and c.type = 'laptop')
group by maker

--20
select distinct c.maker, count(model)
from product c
where c.type = 'pc'
group by maker
having count(model) >= 3

--21
select c.maker, max(a.price)
from product c
         join pc a on (c.model = a.model)
group by maker

--22
select distinct speed, avg(price)
from pc
group by speed
having speed > 600

--23
select c.maker
from product c
where c.model in (select model from pc where speed >= 750)
intersect
select c.maker
from product c
where c.model in (select model from laptop where speed >= 750)

--24
select model
from (
         select model, price
         from pc
         union
         select model, price
         from laptop
         union
         select model, price
         from printer
     ) x
where x.price in (
    select max(price)
    from (
             select price
             from pc
             union
             select price
             from laptop
             union
             select price
             from printer
         ) y)

--25
select distinct maker
from product c
where c.maker in (select maker
                  from product
                  where type = 'printer')
  and c.model in (
    select model
    from pc s
    where s.ram = (select min(ram) from pc)
      and s.speed = (select max(speed)
                     from pc
                     where ram = (select min(ram) from pc))
)

--26
select sum(price) / count(*)
from (
         select price
         from pc s
         where s.model in (select model from product where maker = 'A')
         union
         select price
         from laptop s
         where s.model in (select model from product where maker = 'A')
     ) x


--27
select maker, avg(l.hd)
from product s
         join pc l on s.model = l.model
where maker in (select maker
                from product
                where type = 'printer')
group by maker

--28
select count(*)
from (select count(maker) as count
      from product
      group by maker
      having count (maker) = 1
     ) as qty

--29
select a.point, a.date, a.inc, b.out
from income_o a
         left join outcome_o b on (a.date = b.date and a.point = b.point)
union
select b.point, b.date, a.inc, b.out
from income_o a
         right join outcome_o b on (a.date = b.date and a.point = b.point)

--31
select class, country
from classes
where bore >= 16

--33
select ship
from outcomes
where result = 'sunk'
  and battle = 'North Atlantic'

--34
select distinct name
from ships s
where launched >= 1922
  and s.class in (
    select class
    from classes
    where displacement > 35000
      and type = 'bb'
)
--35
select model, type
from product
where model not like '%[^A-Z]%'
   or model not like '%[^0-9]%'
   or model not like '%[^a-z]%'

--36
select name
from ships
where name = class
union
select ship
from outcomes
where ship in (select class from classes)

--38
select country
from classes
where type = 'bb'
intersect
select country
from classes
where type = 'bc'

--42
select s.ship, l.name
from outcomes s
         join battles l on (result = 'sunk' and s.battle = l.name)

--44
select name
from ships
where name like 'R%'

union
select ship
from outcomes
where ship like 'R%'

--45
select name
from ships
where name like '% % %'
union
select ship
from outcomes
where ship like '% % %'

--49
select name
from ships s
where s.class in (
    select class
    from classes
    where bore = 16)
union
select ship
from outcomes s
where s.ship in (
    select class
    from classes
    where bore = 16)

--50
select distinct battle
from outcomes
where ship in (
    select name
    from ships
    where class = 'Kongo')
