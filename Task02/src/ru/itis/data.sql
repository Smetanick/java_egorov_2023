insert into client (first_name, last_name, age)
values ('Марсель', 'Сидиков', 15),
       ('Кристина', 'Коржуева', 11),
       ('Данил', 'Степкин', 19);

insert into car (color, number, brand)
values ('black', 'o000oo', 'Lada Kalina'),
       ('white', 'o666oo', 'Nissan Skyline'),
       ('red', 'a999aa', 'Porsche Cayenne');

insert into driver (first_name, last_name, car_id, age, phone_number)
values ('Костя', 'Егоров', 3, 1, '88005353535'),
       ('Виталий', 'Комиссаров', 1, 55, '89000000000'),
       ('Андрей', 'Бебур', 2, 99, '89999999999');

insert into orders (id_client, id_driver, car_number, first_place, last_place)
values (1, 3, 'o666oo', 'Москва', 'Турция'),
       (3, 2, 'o000oo', 'Казань', 'Чебоксары'),
       (2, 1, 'a999aa', 'Салават', 'Питер');

update driver
set phone_number = '89111111111'
where phone_number = '89000000000';

update orders
set id_client = 1, first_place = 'Сеул'
where car_number = 'a999aa';