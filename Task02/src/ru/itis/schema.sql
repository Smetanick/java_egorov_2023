drop table if exists client ;
drop table if exists driver;
drop table if exists orders;
drop table if exists car;

create table client (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    age int check ( age < 140 )
);

create table driver (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    car_id bigint,
    age int check ( age < 140 ),
    phone_number char(11) unique
);

create table car (
    car_id bigserial primary key ,
    color char(20),
    number char(6) unique,
    brand char(20)
);

create table orders (
    id bigserial primary key ,
    id_client bigint,
    id_driver bigint,
    car_number char(20),
    first_place char(20),
    last_place char(20)
);

alter table driver
    add foreign key (car_id) references car(car_id);

alter table orders
    add foreign key (id_client) references client(id);

alter table orders
    add foreign key (id_driver) references driver(id);

alter table orders
    add foreign key (car_number) references car(number);