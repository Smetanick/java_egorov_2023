package ru.itis.services;

import ru.itis.dto.StudentSignUp;

public interface StudentsService {
    void signUp(StudentSignUp form);
}
