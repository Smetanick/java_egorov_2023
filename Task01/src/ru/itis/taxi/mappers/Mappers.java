package ru.itis.taxi.mappers;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.models.User;

import java.util.UUID;

public class Mappers {
    public static User fromSignUpForm(SignUpForm form) {
        return new User(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
    }
    public static User splitStringToUser(String string) {
        String[] userdata = string.split("\\|");
        return new User(UUID.fromString(userdata[0]), userdata[1], userdata[2], userdata[3], userdata[4]);
    }
}

