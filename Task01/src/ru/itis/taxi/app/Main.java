package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));

        usersService.signUp(new SignUpForm("андрей", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));

        usersService.signUp(new SignUpForm("костяь", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        List<User> allUsers = usersRepository.findAll();
        System.out.println(allUsers);
        usersRepository.delete(new User("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersRepository.update(new User(allUsers.get(1).getId(), "петя", "сидиков", "petyshok@gmail.com", "1"));
        System.out.println(usersRepository.findById(allUsers.get(2).getId()));
        usersRepository.deleteById(allUsers.get(2).getId());
    }
}

