package ru.itis.taxi.repositories;

import java.util.List;
import java.util.Optional;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CrudRepository<ID, T> {
    List<T> findAll();

    void save(T entity);

    void update(T entity);

    void delete(T entity);

    void deleteById(ID id);

    T findById(ID id);

}

