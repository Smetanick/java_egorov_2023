package ru.itis.services;

import ru.itis.dto.StudentSignUp;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword());

        studentsRepository.save(student);
    }
}

