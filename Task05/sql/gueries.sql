-- получение всех данных из таблицы
select *
from student;

-- получение данных из таблицы, отсортированных по убыванию возраста (если совпали возраста - по возрастанию id)
select *
from student
order by age desc, id;

-- получение только имени и фамилии
select first_name, last_name
from student;

-- получить названия курсов, на которые записан Марсель
select c.title as course_title
from course c
where c.id in (
    select sc.course_id
    from student_course sc
    where sc.student_id in (select s.id from student s where s.first_name = 'Айрат'));

-- получить уникальные названия уроков
select distinct(lesson.name)
from lesson;

-- получить названия уроков и их количество в общем списке уроков
select l.name as lesson_name, count(*) as count
from lesson l
group by l.name;

-- JOINS

-- получить все курсы и их уроки
select *
from course c
         left join lesson l on c.id = l.course_id;

-- получить все уроки и их курсы
select *
from course c
         right join lesson l on c.id = l.course_id;

-- получить пересечения курсов и уроков
select *
from course c
         inner join lesson l on c.id = l.course_id;

select *
from course c
         full outer join lesson l on c.id = l.course_id;

select *
from student
where id = 4;
